var jsdom = require('jsdom');
var http = require('http');
var Cache = require('expiring-lru-cache');
var utils = require('./utils');
var rest = require('restler');
var uuid = require('node-uuid');
var moment = require('moment');
var _ = require('underscore');
var xmlParser = require('libxml-to-js');

// http://annttu.fi/vr/#H9605
// -- MANUALLY ADDED: JRS
var stationCodeToCity = {};
stationCodeToCity['MIS'] = 'Misi';
stationCodeToCity['PTI'] = 'Paimenportti';
stationCodeToCity['PTO'] = 'Paltamo';
stationCodeToCity['MNK'] = 'Mankki';
stationCodeToCity['ALV'] = 'Alavus';
stationCodeToCity['SIJ'] = 'Siilinjärvi';
stationCodeToCity['PTS'] = 'Pietarsaari';
stationCodeToCity['PKO'] = 'Parkano';
stationCodeToCity['JY'] = 'Jyväskylä';
stationCodeToCity['VNA'] = 'Vainikkala';
stationCodeToCity['VNJ'] = 'Viinijärvi';
stationCodeToCity['JJ'] = 'Juupajoki';
stationCodeToCity['JK'] = 'Jokela';
stationCodeToCity['MLÄ'] = 'Mäntsälä';
stationCodeToCity['HL'] = 'Hämeenlinna';
stationCodeToCity['VAA'] = 'Vaala';
stationCodeToCity['VLP'] = 'Vilppula';
stationCodeToCity['VAR'] = 'Varkaus';
stationCodeToCity['VIA'] = 'Viiala';
stationCodeToCity['ÄHT'] = 'Ähtäri';
stationCodeToCity['MI'] = 'Mikkeli';
stationCodeToCity['NUP'] = 'Nuppulinna';
stationCodeToCity['MH'] = 'Muhos';
stationCodeToCity['PVI'] = 'Petäjävesi';
stationCodeToCity['VMA'] = 'Vammala';
stationCodeToCity['LNA'] = 'Lapinlahti';
stationCodeToCity['PM'] = 'Pieksämäki';
stationCodeToCity['LUS'] = 'Lusto';
stationCodeToCity['JTS'] = 'Joutseno';
stationCodeToCity['KUO'] = 'Kuopio';
stationCodeToCity['MUL'] = 'Muurola';
stationCodeToCity['IKR'] = 'Inkeroinen';
stationCodeToCity['KEM'] = 'Kemi';
stationCodeToCity['HKS'] = 'Hankasalmi';
stationCodeToCity['HKP'] = 'Hanko-Pohjoinen';
stationCodeToCity['SLK'] = 'Savonlinna-Kauppatori';
stationCodeToCity['JP'] = 'Järvenpää';
stationCodeToCity['JR'] = 'Järvelä';
stationCodeToCity['HKH'] = 'Hiekkaharju';
stationCodeToCity['KEA'] = 'Kera';
stationCodeToCity['KEU'] = 'Keuruu';
stationCodeToCity['IKO'] = 'Inkoo';
stationCodeToCity['JNS'] = 'Joensuu';
stationCodeToCity['KJÄ'] = 'Kemijärvi';
stationCodeToCity['PSL'] = 'Pasila';
stationCodeToCity['ILA'] = 'Ilmala';
stationCodeToCity['KRÖ'] = 'Kyrölä';
stationCodeToCity['SGY'] = 'Skogby';
stationCodeToCity['KOK'] = 'Kokkola';
stationCodeToCity['TMS'] = 'Tammisaari';
stationCodeToCity['HVA'] = 'Harjavalta';
stationCodeToCity['UIM'] = 'Uimaharju';
stationCodeToCity['POH'] = 'Pohjois-Haaga';
stationCodeToCity['RKL'] = 'Rekola';
stationCodeToCity['SLO'] = 'Salo';
stationCodeToCity['UKÄ'] = 'Uusikylä';
stationCodeToCity['PH'] = 'Pihlajavesi';
stationCodeToCity['NRM'] = 'Nurmes';
stationCodeToCity['SK'] = 'Seinäjoki';
stationCodeToCity['MLA'] = 'Mommila';
stationCodeToCity['ML'] = 'Malmi';
stationCodeToCity['KLN'] = 'Kyminlinna';
stationCodeToCity['KLH'] = 'Kauklahti';
stationCodeToCity['MLO'] = 'Malminkartano';
stationCodeToCity['RI'] = 'Riihimäki';
stationCodeToCity['TNA'] = 'Tapanila';
stationCodeToCity['MR'] = 'Mäntyharju';
stationCodeToCity['LR'] = 'Lappeenranta';
stationCodeToCity['MY'] = 'Myllymäki';
stationCodeToCity['KYN'] = 'Kylänlahti';
stationCodeToCity['VLH'] = 'Villähde';
stationCodeToCity['TOR'] = 'Tornio';
stationCodeToCity['LM'] = 'Loimaa';
stationCodeToCity['LH'] = 'Lahti';
stationCodeToCity['SPL'] = 'Simpele';
stationCodeToCity['TOL'] = 'Tolsa';
stationCodeToCity['YV'] = 'Ylivieska';
stationCodeToCity['PAR'] = 'Parikkala';
stationCodeToCity['KAJ'] = 'Kajaani';
stationCodeToCity['IKY'] = 'Isokyrö';
stationCodeToCity['ENO'] = 'Eno';
stationCodeToCity['MAS'] = 'Masala';
stationCodeToCity['LAI'] = 'Laihia';
stationCodeToCity['UTJ'] = 'Utajärvi';
stationCodeToCity['PHÄ'] = 'Pyhäsalmi';
stationCodeToCity['ILM'] = 'Iisalmi';
stationCodeToCity['SNJ'] = 'Suonenjoki';
stationCodeToCity['LAA'] = 'Lappila';
stationCodeToCity['KIT'] = 'Kitee';
stationCodeToCity['VS'] = 'Vaasa';
stationCodeToCity['KIL'] = 'Kilo';
stationCodeToCity['OVK'] = 'Orivesi keskusta';
stationCodeToCity['MÄK'] = 'Mäkkylä';
stationCodeToCity['EPZ'] = 'Eläinpuisto-Zoo';
stationCodeToCity['KNI'] = 'Kauniainen';
stationCodeToCity['SL'] = 'Savonlinna';
stationCodeToCity['JÄS'] = 'Jämsä';
stationCodeToCity['ITA'] = 'Iittala';
stationCodeToCity['VTI'] = 'Vihanti';
stationCodeToCity['TSL'] = 'Tavastila';
stationCodeToCity['SKV'] = 'Sukeva';
stationCodeToCity['HAA'] = 'Haarajoki';
stationCodeToCity['PRI'] = 'Pori';
stationCodeToCity['HAU'] = 'Haukivuori';
stationCodeToCity['PJM'] = 'Pitäjänmäki';
stationCodeToCity['RNN'] = 'Runni';
stationCodeToCity['HPL'] = 'Huopalahti';
stationCodeToCity['NOA'] = 'Nokia';
stationCodeToCity['KKN'] = 'Kirkkonummi';
stationCodeToCity['KKI'] = 'Kokemäki';
stationCodeToCity['NVL'] = 'Nivala';
stationCodeToCity['OL'] = 'Oulu';
stationCodeToCity['OU'] = 'Oulainen';
stationCodeToCity['OLK'] = 'Oulunkylä';
stationCodeToCity['TPE'] = 'Tampere';
stationCodeToCity['YST'] = 'Ylistaro';
stationCodeToCity['ROI'] = 'Rovaniemi';
stationCodeToCity['HY'] = 'Hyvinkää';
stationCodeToCity['TKL'] = 'Tikkurila';
stationCodeToCity['RY'] = 'Ryttylä';
stationCodeToCity['RKI'] = 'Ruukki';
stationCodeToCity['PEL'] = 'Pello';
stationCodeToCity['HKI'] = 'Helsinki';
stationCodeToCity['LPÄ'] = 'Lempäälä';
stationCodeToCity['HNK'] = 'Hanko';
stationCodeToCity['REE'] = 'Retretti';
stationCodeToCity['TRI'] = 'Tornio-Itäinen';
stationCodeToCity['PUN'] = 'Punkaharju';
stationCodeToCity['VKS'] = 'Vantaankoski';
stationCodeToCity['TRL'] = 'Tuomarila';
stationCodeToCity['TUU'] = 'Tuuri';
stationCodeToCity['EPO'] = 'Espoo';
stationCodeToCity['PLA'] = 'Puistola';
stationCodeToCity['DRA'] = 'Dragsvik';
stationCodeToCity['LMA'] = 'Luoma';
stationCodeToCity['PUR'] = 'Purola';
stationCodeToCity['TRV'] = 'Tervola';
stationCodeToCity['OI'] = 'Oitti';
stationCodeToCity['KRS'] = 'Korso';
stationCodeToCity['KRV'] = 'Kiuruvesi';
stationCodeToCity['KRU'] = 'Karkku';
stationCodeToCity['TKU'] = 'Turku';
stationCodeToCity['OV'] = 'Orivesi';
stationCodeToCity['KRA'] = 'Koria';
stationCodeToCity['NSL'] = 'Nastola';
stationCodeToCity['IMR'] = 'Imatra';
stationCodeToCity['KIÄ'] = 'Kerimäki';
stationCodeToCity['KOH'] = 'Kohtavaara';
stationCodeToCity['KON'] = 'Kontiomäki';
stationCodeToCity['LOH'] = 'Louhela';
stationCodeToCity['MKI'] = 'Myllykoski';
stationCodeToCity['KUT'] = 'Kupittaa';
stationCodeToCity['HR'] = 'Herrala';
stationCodeToCity['HP'] = 'Humppila';
stationCodeToCity['KLO'] = 'Kolho';
stationCodeToCity['KLI'] = 'Kolari';
stationCodeToCity['HK'] = 'Hikiä';
stationCodeToCity['PNÄ'] = 'Pännäinen';
stationCodeToCity['LVT'] = 'Lievestuore';
stationCodeToCity['STA'] = 'Santala';
stationCodeToCity['TUS'] = 'Turku satama';
stationCodeToCity['STI'] = 'Siuntio';
stationCodeToCity['KTS'] = 'Kotkan satama';
stationCodeToCity['KTI'] = 'Kesälahti';
stationCodeToCity['KTA'] = 'Kotka';
stationCodeToCity['YTR'] = 'Ylitornio';
stationCodeToCity['PRL'] = 'Parola';
stationCodeToCity['HÖL'] = 'Höljäkkä';
stationCodeToCity['MYR'] = 'Myyrmäki';
stationCodeToCity['KHA'] = 'Kauhava';
stationCodeToCity['SAV'] = 'Savio';
stationCodeToCity['SAU'] = 'Saunakallio';
stationCodeToCity['TU'] = 'Turenki';
stationCodeToCity['HPK'] = 'Haapamäki';
stationCodeToCity['HPJ'] = 'Haapajärvi';
stationCodeToCity['TL'] = 'Toijala';
stationCodeToCity['TK'] = 'Tervajoki';
stationCodeToCity['LIS'] = 'Lieksa';
stationCodeToCity['PMK'] = 'Pukinmäki';
stationCodeToCity['KAN'] = 'Kannelmäki';
stationCodeToCity['KÄP'] = 'Käpylä';
stationCodeToCity['KNS'] = 'Kannus';
stationCodeToCity['KVY'] = 'Koivukylä';
stationCodeToCity['KA'] = 'Kausala';
stationCodeToCity['KE'] = 'Kerava';
stationCodeToCity['KR'] = 'Karjaa';
stationCodeToCity['LPV'] = 'Leppävaara';
stationCodeToCity['LPO'] = 'Lappohja';
stationCodeToCity['KV'] = 'Kouvola';
stationCodeToCity['HNV'] = 'Heinävesi';
stationCodeToCity['KY'] = 'Kymi';
stationCodeToCity['VSL'] = 'Vuonislahti';
stationCodeToCity['LPA'] = 'Lapua';
// Added manually:
stationCodeToCity['JRS'] = 'Jorvas';
stationCodeToCity['VMO'] = 'Valimo';
stationCodeToCity['KVH'] = 'Koivuhovi';

var stationCityToCode = {"Misi":"MIS","Paimenportti":"PTI","Paltamo":"PTO","Mankki":"MNK","Alavus":"ALV","Siilinjärvi":"SIJ","Pietarsaari":"PTS","Parkano":"PKO","Jyväskylä":"JY","Vainikkala":"VNA","Viinijärvi":"VNJ","Juupajoki":"JJ","Jokela":"JK","Mäntsälä":"MLÄ","Hämeenlinna":"HL","Vaala":"VAA","Vilppula":"VLP","Varkaus":"VAR","Viiala":"VIA","Ähtäri":"ÄHT","Mikkeli":"MI","Nuppulinna":"NUP","Muhos":"MH","Petäjävesi":"PVI","Vammala":"VMA","Lapinlahti":"LNA","Pieksämäki":"PM","Lusto":"LUS","Joutseno":"JTS","Kuopio":"KUO","Muurola":"MUL","Inkeroinen":"IKR","Kemi":"KEM","Hankasalmi":"HKS","Hanko-Pohjoinen":"HKP","Savonlinna-Kauppatori":"SLK","Järvenpää":"JP","Järvelä":"JR","Hiekkaharju":"HKH","Kera":"KEA","Keuruu":"KEU","Inkoo":"IKO","Joensuu":"JNS","Kemijärvi":"KJÄ","Pasila":"PSL","Ilmala":"ILA","Kyrölä":"KRÖ","Skogby":"SGY","Kokkola":"KOK","Tammisaari":"TMS","Harjavalta":"HVA","Uimaharju":"UIM","Pohjois-Haaga":"POH","Rekola":"RKL","Salo":"SLO","Uusikylä":"UKÄ","Pihlajavesi":"PH","Nurmes":"NRM","Seinäjoki":"SK","Mommila":"MLA","Malmi":"ML","Kyminlinna":"KLN","Kauklahti":"KLH","Malminkartano":"MLO","Riihimäki":"RI","Tapanila":"TNA","Mäntyharju":"MR","Lappeenranta":"LR","Myllymäki":"MY","Kylänlahti":"KYN","Villähde":"VLH","Tornio":"TOR","Loimaa":"LM","Lahti":"LH","Simpele":"SPL","Tolsa":"TOL","Ylivieska":"YV","Parikkala":"PAR","Kajaani":"KAJ","Isokyrö":"IKY","Eno":"ENO","Masala":"MAS","Laihia":"LAI","Utajärvi":"UTJ","Pyhäsalmi":"PHÄ","Iisalmi":"ILM","Suonenjoki":"SNJ","Lappila":"LAA","Kitee":"KIT","Vaasa":"VS","Kilo":"KIL","Orivesi keskusta":"OVK","Mäkkylä":"MÄK","Eläinpuisto-Zoo":"EPZ","Kauniainen":"KNI","Savonlinna":"SL","Jämsä":"JÄS","Iittala":"ITA","Vihanti":"VTI","Tavastila":"TSL","Sukeva":"SKV","Haarajoki":"HAA","Pori":"PRI","Haukivuori":"HAU","Pitäjänmäki":"PJM","Runni":"RNN","Huopalahti":"HPL","Nokia":"NOA","Kirkkonummi":"KKN","Kokemäki":"KKI","Nivala":"NVL","Oulu":"OL","Oulainen":"OU","Oulunkylä":"OLK","Tampere":"TPE","Ylistaro":"YST","Rovaniemi":"ROI","Hyvinkää":"HY","Tikkurila":"TKL","Ryttylä":"RY","Ruukki":"RKI","Pello":"PEL","Helsinki":"HKI","Lempäälä":"LPÄ","Hanko":"HNK","Retretti":"REE","Tornio-Itäinen":"TRI","Punkaharju":"PUN","Vantaankoski":"VKS","Tuomarila":"TRL","Tuuri":"TUU","Espoo":"EPO","Puistola":"PLA","Dragsvik":"DRA","Luoma":"LMA","Purola":"PUR","Tervola":"TRV","Oitti":"OI","Korso":"KRS","Kiuruvesi":"KRV","Karkku":"KRU","Turku":"TKU","Orivesi":"OV","Koria":"KRA","Nastola":"NSL","Imatra":"IMR","Kerimäki":"KIÄ","Kohtavaara":"KOH","Kontiomäki":"KON","Louhela":"LOH","Myllykoski":"MKI","Kupittaa":"KUT","Herrala":"HR","Humppila":"HP","Kolho":"KLO","Kolari":"KLI","Hikiä":"HK","Pännäinen":"PNÄ","Lievestuore":"LVT","Santala":"STA","Turku satama":"TUS","Siuntio":"STI","Kotkan satama":"KTS","Kesälahti":"KTI","Kotka":"KTA","Ylitornio":"YTR","Parola":"PRL","Höljäkkä":"HÖL","Myyrmäki":"MYR","Kauhava":"KHA","Savio":"SAV","Saunakallio":"SAU","Turenki":"TU","Haapamäki":"HPK","Haapajärvi":"HPJ","Toijala":"TL","Tervajoki":"TK","Lieksa":"LIS","Pukinmäki":"PMK","Kannelmäki":"KAN","Käpylä":"KÄP","Kannus":"KNS","Koivukylä":"KVY","Kausala":"KA","Kerava":"KE","Karjaa":"KR","Leppävaara":"LPV","Lappohja":"LPO","Kouvola":"KV","Heinävesi":"HNV","Kymi":"KY","Vuonislahti":"VSL","Lapua":"LPA","Jorvas":"JRS","Valimo":"VMO","Koivuhovi":"KVH"};
var stationCoordinates = {"KVH":{"lat":60.2071474132788,"lon":24.7012687033073},"VMO":{"lat":60.2221706066547,"lon":24.8758168247687},"JRS":{"lat":60.1378946392413,"lon":24.5127813696834},"LPA":{"lat":62.9713382769334,"lon":23.0151218150812},"VSL":{"lat":63.1526331577559,"lon":29.9933446623504},"KY":{"lat":60.5391160320611,"lon":26.9069885395217},"HNV":{"lat":62.3825421542214,"lon":28.6433876159328},"KV":{"lat":60.8655426741245,"lon":26.7032673776803},"LPO":{"lat":59.9040553435155,"lon":23.2367477390249},"LPV":{"lat":60.2194802336929,"lon":24.8134577450556},"KR":{"lat":60.0686205974971,"lon":23.6609497513004},"KE":{"lat":60.4025021488988,"lon":25.1055869101125},"KA":{"lat":60.8853191308073,"lon":26.3295656053891},"KVY":{"lat":60.3235212762024,"lon":25.0600059802696},"KNS":{"lat":63.8984315659718,"lon":23.9153823380664},"KÄP":{"lat":60.2203550243764,"lon":24.9464797333388},"KAN":{"lat":60.2395608232772,"lon":24.876999501756},"PMK":{"lat":60.242413491032,"lon":24.9936599437636},"LIS":{"lat":63.318198405343,"lon":30.0164219508081},"TK":{"lat":63.0007767284291,"lon":22.1714621079558},"TL":{"lat":61.1705965206429,"lon":23.8608235186115},"HPJ":{"lat":63.7545205391405,"lon":25.3343637030939},"HPK":{"lat":62.2464130806991,"lon":24.4551270905068},"TU":{"lat":60.9184682054281,"lon":24.638553612264},"SAU":{"lat":60.487284553531,"lon":25.0655763884306},"SAV":{"lat":60.3814262233465,"lon":25.0978503323231},"KHA":{"lat":63.0996479779108,"lon":23.0352852831675},"MYR":{"lat":60.2613242751813,"lon":24.8547359791479},"HÖL":{"lat":63.4548742738465,"lon":29.4441411260754},"PRL":{"lat":61.050023039725,"lon":24.366320485935},"YTR":{"lat":66.3253319998701,"lon":23.6832041560085},"KTA":{"lat":60.4643797813782,"lon":26.9286067753117},"KTI":{"lat":61.887466526836,"lon":29.8028751511363},"KTS":{"lat":60.468711537983,"lon":26.9404336053719},"STI":{"lat":60.1406547501066,"lon":24.2217515170571},"TUS":{"lat":60.4344209751372,"lon":22.2249554365505},"STA":{"lat":59.875072842205,"lon":23.1213189681202},"LVT":{"lat":62.2599100487137,"lon":26.1961652556913},"PNÄ":{"lat":63.5956596099097,"lon":22.7911741925907},"HK":{"lat":60.7525846819442,"lon":24.9192030904225},"KLI":{"lat":67.3487068866755,"lon":23.8362679265897},"KLO":{"lat":62.1280493830283,"lon":24.507437049513},"HP":{"lat":60.9195087412278,"lon":23.3627655360884},"HR":{"lat":60.8969383594794,"lon":25.4756038568887},"KUT":{"lat":60.4502184006399,"lon":22.2971777476904},"MKI":{"lat":60.7700325560227,"lon":26.787597784989},"LOH":{"lat":60.2708048660607,"lon":24.8532848496211},"KON":{"lat":64.3382209993014,"lon":28.1147135483404},"KOH":{"lat":63.5151282595281,"lon":29.2842921106587},"KIÄ":{"lat":61.8614028463005,"lon":29.1140729705515},"IMR":{"lat":61.1960347364294,"lon":28.7763212908405},"NSL":{"lat":60.9355620325492,"lon":25.9354497170809},"KRA":{"lat":60.8474425454102,"lon":26.6022394721459},"OV":{"lat":61.6500999453772,"lon":24.368040220715},"TKU":{"lat":60.4540626965237,"lon":22.252813854496},"KRU":{"lat":61.4433175911428,"lon":23.0491242792771},"KRV":{"lat":63.6409144395118,"lon":26.6113373847646},"KRS":{"lat":60.3509325880371,"lon":25.0782267517225},"OI":{"lat":60.7898091491224,"lon":25.0213042401763},"TRV":{"lat":66.0818108064357,"lon":24.7722940012463},"PUR":{"lat":60.5002583122918,"lon":25.050270730923},"LMA":{"lat":60.1731050401385,"lon":24.5508733692944},"DRA":{"lat":59.9902253416585,"lon":23.4886538410037},"PLA":{"lat":60.276599068651,"lon":25.0367970886227},"EPO":{"lat":60.2050697178862,"lon":24.6559798867302},"TUU":{"lat":62.6053592372922,"lon":23.7223172256872},"TRL":{"lat":60.206054027033,"lon":24.6817589572196},"VKS":{"lat":60.2856766141148,"lon":24.8482533138495},"PUN":{"lat":61.7611459961654,"lon":29.3823578574763},"TRI":{"lat":65.8507983218434,"lon":24.1849867863632},"REE":{"lat":61.8033020344276,"lon":29.2942868293918},"HNK":{"lat":59.8268631302601,"lon":22.968289872509},"LPÄ":{"lat":61.3142915497061,"lon":23.7550914862771},"HKI":{"lat":60.1720917396682,"lon":24.9412457352685},"PEL":{"lat":66.7846580655563,"lon":23.9933866294813},"RKI":{"lat":64.6603667264114,"lon":25.0982084816437},"RY":{"lat":60.8181540074306,"lon":24.7581206881206},"TKL":{"lat":60.2924319777477,"lon":25.0441779254748},"HY":{"lat":60.6314568858326,"lon":24.8574407076537},"ROI":{"lat":66.4976733332296,"lon":25.7053367438949},"YST":{"lat":62.9178108284092,"lon":22.5327238574789},"TPE":{"lat":61.4976117284836,"lon":23.7740283973695},"OLK":{"lat":60.2292125439701,"lon":24.9680810179195},"OU":{"lat":64.2694730635655,"lon":24.8209998295012},"OL":{"lat":65.0123980716845,"lon":25.4861387415853},"NVL":{"lat":63.9363903683797,"lon":24.9607835267595},"KKI":{"lat":61.2541809966044,"lon":22.3034295998596},"KKN":{"lat":60.1196405307238,"lon":24.4389860447007},"NOA":{"lat":61.4810638150964,"lon":23.4991709390577},"HPL":{"lat":60.218365990445,"lon":24.8934628002039},"RNN":{"lat":63.6020548214946,"lon":26.8865028008271},"PJM":{"lat":60.2233884103417,"lon":24.859771422591},"HAU":{"lat":62.0256543118971,"lon":27.2026158709574},"PRI":{"lat":61.4772173843826,"lon":21.7865966572185},"HAA":{"lat":60.496255392329,"lon":25.133809515854},"SKV":{"lat":63.8646120423051,"lon":27.4223116207827},"TSL":{"lat":60.5728925381998,"lon":26.9542231070042},"VTI":{"lat":64.4845180676582,"lon":24.9936450586793},"ITA":{"lat":61.090582532081,"lon":24.140831707455},"JÄS":{"lat":61.8656316150579,"lon":25.1732702065982},"SL":{"lat":61.8690566079375,"lon":28.8862334357201},"KNI":{"lat":60.2119390946602,"lon":24.7308581218048},"EPZ":{"lat":62.5409780937739,"lon":24.1903782869506},"MÄK":{"lat":60.2213059911276,"lon":24.8429694862304},"OVK":{"lat":61.6739091855047,"lon":24.3698320093638},"KIL":{"lat":60.217935119038,"lon":24.7823960686559},"VS":{"lat":63.0978369498149,"lon":21.621702694183},"KIT":{"lat":62.1428645925832,"lon":30.0491055354533},"LAA":{"lat":60.8489550377593,"lon":25.1742362458716},"SNJ":{"lat":62.6237152019544,"lon":27.1245055963053},"ILM":{"lat":63.5601071505716,"lon":27.2011152262921},"PHÄ":{"lat":63.6829397769137,"lon":25.9826423152734},"UTJ":{"lat":64.7589302078738,"lon":26.4135783684134},"LAI":{"lat":62.9901484511189,"lon":22.0034385941334},"MAS":{"lat":60.1586587763578,"lon":24.5391015403841},"ENO":{"lat":62.8063772669732,"lon":30.1417109895974},"IKY":{"lat":62.9510855113052,"lon":22.3947955585747},"KAJ":{"lat":64.2197172795586,"lon":27.7385659060757},"PAR":{"lat":61.5585493678418,"lon":29.5028831752296},"YV":{"lat":64.0719084706505,"lon":24.5406082756271},"TOL":{"lat":60.1176408999754,"lon":24.4702512523943},"SPL":{"lat":61.4247849443425,"lon":29.3717002269792},"LH":{"lat":60.9764738866518,"lon":25.6572467017841},"LM":{"lat":60.8473989167806,"lon":23.0604068677173},"TOR":{},"VLH":{"lat":60.9466302666584,"lon":25.8249414861113},"KYN":{"lat":63.3613924673245,"lon":29.751621134797},"MY":{"lat":62.5247548373713,"lon":24.2809648134262},"LR":{"lat":61.0476065142242,"lon":28.195077199272},"MR":{"lat":61.4211051646656,"lon":26.8817497427936},"TNA":{"lat":60.2641484505336,"lon":25.0302669204121},"RI":{"lat":60.7361307032109,"lon":24.7812247284075},"MLO":{"lat":60.2493233553054,"lon":24.8612641620817},"KLH":{"lat":60.1894501477597,"lon":24.6003376990427},"KLN":{"lat":60.5083470354029,"lon":26.8955507132172},"ML":{"lat":60.2518636271413,"lon":25.011838718977},"MLA":{"lat":60.8211726293947,"lon":25.0751269800873},"SK":{"lat":62.7919880445709,"lon":22.844227177813},"NRM":{"lat":63.5410806691513,"lon":29.1372475465544},"PH":{"lat":62.3462247070028,"lon":24.3955920894681},"UKÄ":{"lat":60.9276894974567,"lon":25.9951304931306},"SLO":{"lat":60.3846131663948,"lon":23.1216922720197},"RKL":{"lat":60.3331035932356,"lon":25.0688235466544},"POH":{"lat":60.2300817843473,"lon":24.8832494405138},"UIM":{"lat":62.9124713070708,"lon":30.2415058046907},"HVA":{"lat":61.3097003697258,"lon":22.1332506144457},"TMS":{"lat":59.9795470484717,"lon":23.4434122398778},"KOK":{"lat":63.8344384637995,"lon":23.1310904555053},"SGY":{"lat":59.926014611743,"lon":23.3106180643507},"KRÖ":{"lat":60.4567606522757,"lon":25.1015090716606},"ILA":{"lat":60.2081567808029,"lon":24.9205839896604},"PSL":{"lat":60.199054881001,"lon":24.9333345869716},"KJÄ":{"lat":66.7242759424658,"lon":27.4037352242751},"JNS":{"lat":62.5995333454642,"lon":29.7770562044315},"IKO":{"lat":60.0615127464171,"lon":23.935824472537},"KEU":{"lat":62.255309245685,"lon":24.7073148861924},"KEA":{"lat":60.2165702785059,"lon":24.7556295250905},"HKH":{"lat":60.303345928025,"lon":25.0492146984245},"JR":{"lat":60.8673271430942,"lon":25.2746583365616},"JP":{"lat":60.4736462568726,"lon":25.0908525460913},"HKP":{"lat":59.8301519801011,"lon":22.9883958484171},"HKS":{"lat":62.3028174189374,"lon":26.4781367664871},"KEM":{"lat":65.736706203287,"lon":24.5743921976806},"IKR":{"lat":60.6967925965661,"lon":26.8389649800709},"MUL":{"lat":66.3708753946856,"lon":25.3751073856932},"KUO":{"lat":62.897220894424,"lon":27.6795406630281},"JTS":{"lat":61.1202910534812,"lon":28.488240903736},"LUS":{"lat":61.8005437266694,"lon":29.3222736534224},"PM":{"lat":62.3011418542859,"lon":27.1675123776504},"LNA":{"lat":63.3606675668124,"lon":27.3935709934478},"VMA":{"lat":61.3495274748078,"lon":22.8876503013428},"PVI":{"lat":62.2569732615613,"lon":25.189339647693},"MH":{"lat":64.8054024824012,"lon":25.9920155615267},"NUP":{"lat":60.5293695500533,"lon":25.0187716737947},"MI":{"lat":61.6878162432883,"lon":27.2779652903648},"ÄHT":{"lat":62.5532679515096,"lon":24.0640359375581},"VIA":{"lat":61.2117248684532,"lon":23.7707179950548},"VAR":{"lat":62.3122781129882,"lon":27.8737114038927},"VLP":{"lat":62.0258959837054,"lon":24.5069526240129},"VAA":{"lat":64.5580173622428,"lon":26.8427111075193},"HL":{"lat":61.0026154759784,"lon":24.4782874731282},"MLÄ":{"lat":60.6472564851628,"lon":25.3074652329458},"JK":{"lat":60.5528446571929,"lon":24.9705586882541},"JJ":{"lat":61.7967960324578,"lon":24.365042201252},"VNJ":{"lat":62.6416863233859,"lon":29.2371844481763},"VNA":{"lat":60.8638288370666,"lon":28.3032018341785},"JY":{"lat":62.2403951514995,"lon":25.7526952070214},"PKO":{"lat":62.0517776893819,"lon":23.0784636067994},"SIJ":{"lat":63.0749576277382,"lon":27.666564732311},"ALV":{"lat":62.6177746696382,"lon":23.6004278431244},"MNK":{"lat":60.1852574352316,"lon":24.5830801641194},"PTO":{"lat":64.4006131877315,"lon":27.8221873449959},"PTI":{"lat":60.4759368278471,"lon":26.9193401500995},"MIS":{"lat":66.6179278559527,"lon":26.6872540812908}};

/*
 // http://www.vr.fi/fi/index/palvelut/palvelutasemilla/asemantiedot.html.stx
 // --> iframe --> http://ext-service.vr.fi/asemantiedot/goto.do??language=FI&datagroupid=1

 ^ these are bad. not using ääkköset
 var stationCodeToCity = {};
 var stationCityToCode = {};
 $('#asemanTunnus option').each(function(i, el) {
 var $station = $(el);
 if ($station.val().length > 0) {
 var code = $station.val().toUpperCase();
 var city = $station.text();
 stationCityToCode[city] = code;
 stationCodeToCity[code] = city;
 }
 });
 console.log(JSON.stringify(stationCodeToCity));
 */

// http://www.vr.fi/en/index/palvelut/mobiilipalvelut/popup_asemien_tunnukset.html
/*
 var stationCodeToCity = {};
 var stationCityToCode = {};
 $('table tr').each(function(i, el) {
 var $row = $(el);
 var city = $row.find('td:eq(0)').text();
 var code = $row.find('td:eq(1)').text();
 if (city.length > 0 && code.length > 0) {
 stationCityToCode[city] = code;
 stationCodeToCity[code] = city;
 }
 });

 console.log(JSON.stringify(stationCodeToCity));
 */
//var stationCityToCode = {"Alavus":"ALV","Dragsvik":"DRA","Eläinpuisto-Zoo":"EPZ","Eno":"ENO","Espoo":"EPO","Haapajärvi":"HPJ","Haapamäki":"HPK","Haarajoki":"HAA","Hankasalmi":"HKS","Hanko":"HNK","Hanko-Pohjoinen":"HKP","Harjavalta":"HVA","Haukivuori":"HAU","Heinävesi":"HNV","Helsinki":"HKI","Herrala":"HR","Hiekkaharju":"HKH","Hikiä":"HK","Humppila":"HP","Huopalahti":"HPL","Hyvinkää":"HY","Hämeenlinna":"HL","Höljäkkä":"HÖL","Iisalmi":"ILM","Iittala":"ITA","Ilmala":"ILA","Imatra":"IMR","Inkeroinen":"IKR","Inkoo":"IKO","Isokyrö":"IKY","Joensuu":"JNS","Jokela":"JK","Jorvas":"JRS","Joutseno":"JTS","Juupajoki":"JJ","Jyväskylä":"JY","Jämsä":"JÄS","Järvelä":"JR","Järvenpää":"JP","Kajaani":"KAJ","Kannelmäki":"KAN","Kannus":"KNS","Karjaa":"KR","Karkku":"KRU","Kauhava":"KHA","Kauklahti":"KLH","Kauniainen":"KNI","Kausala":"KA","Kemi":"KEM","Kemijärvi":"KJÄ","Kera":"KEA","Kerava":"KE","Kerimäki":"KIÄ","Kesälahti":"KTI","Keuruu":"KEU","Kilo":"KIL","Kirkkonummi":"KKN","Kitee":"KIT","Kiuruvesi":"KRV","Kohtavaara":"KOH","Koivuhovi":"KVH","Koivukylä":"KVY","Kokemäki":"KKI","Kokkola":"KOK","Kolari":"KLI","Kolho":"KLO","Kontiomäki":"KON","Koria":"KRA","Korso":"KRS","Kotka":"KTA","Kotkan satama":"KTS","Kouvola":"KV","Kuopio":"KUO","Kupittaa":"KUT","Kylänlahti":"KYN","Kymi":"KY","Kyminlinna":"KLN","Kyrölä":"KRÖ","Käpylä":"KÄP","Lahti":"LH","Laihia":"LAI","Lapinlahti":"LNA","Lappeenranta":"LR","Lappila":"LAA","Lappohja":"LPO","Lapua":"LPA","Lempäälä":"LPÄ","Leppävaara":"LPV","Lieksa":"LIS","Lievestuore":"LVT","Loimaa":"LM","Louhela":"LOH","Luoma":"LMA","Lusto":"LUS","Malmi":"ML","Malminkartano":"MLO","Mankki":"MNK","Martinlaakso":"MRL","Masala":"MAS","Mikkeli":"MI","Mommila":"MLA","Muhos":"MH","Muurola":"MUL","Myllykoski":"MKI","Myllymäki":"MY","Myyrmäki":"MYR","Mäkkylä":"MÄK","Mäntsälä":"MLÄ","Mäntyharju":"MR","Nastola":"NSL","Nivala":"NVL","Nokia":"NOA","Nuppulinna":"NUP","Nurmes":"NRM","Oitti":"OI","Orivesi":"OV","Orivesi keskusta ":"OVK","Oulainen":"OU","Oulu":"OL","Oulunkylä":"OLK","Paimenportti":"PTI","Paltamo":"PTO","Parikkala":"PAR","Parkano":"PKO","Parola":"PRL","Pasila":"PSL","Pello":"PEL","Petäjävesi":"PVI","Pieksämäki":"PM","Pietarsaari":"PTS","Pihlajavesi":"PH","Pitäjänmäki":"PJM","Pohjois-Haaga":"POH","Pori":"PRI","Puistola":"PLA","Pukinmäki":"PMK","Punkaharju":"PUN","Purola":"PUR","Pyhäsalmi":"PHÄ","Pännäinen":"PNÄ","Pääskylahti ":"PKY ","Rekola":"RKL","Retretti":"REE","Riihimäki":"RI","Rovaniemi":"ROI","Runni":"RNN","Ruukki":"RKI","Ryttylä":"RY","Salo":"SLO","Santala":"STA","Saunakallio":"SAU","Savio":"SAV","Savonlinna":"SL","Seinäjoki":"SK","Siilinjärvi":"SIJ","Simpele":"SPL","Siuntio":"STI","Skogby":"SKY","Sukeva":"SKV","Suonenjoki":"SNJ","Tammisaari":"TMS","Tampere":"TPE","Tapanila":"TNA","Tavastila":"TSL","Tervajoki":"TK","Tervola":"TRV","Tikkurila":"TKL","Toijala":"TL","Tolsa":"TOL","Tornio":"TOR","Tuomarila":"TRL","Turenki":"TU","Turku":"TKU","Turku satama":"TUS","Tuuri":"TUU","Uimaharju":"UIM","Utajärvi":"UTJ","Vaala":"VAA","Vaasa":"VS","Vainikkala":"VNA","Valimo":"VMO","Vammala":"VMA","Vantaankoski":"VKS","Varkaus":"VAR","Vihanti":"VTI","Vihtari":"VIH","Viiala":"VIA","Viinijärvi":"VNJ","Vilppula":"VLP","Vuonislahti":"VSL","Ylistaro":"YST","Ylitornio":"YTR","Ylivieska":"YV","Ähtäri":"ÄHT"};
//var stationCodeToCity = {"ALV":"Alavus","DRA":"Dragsvik","EPZ":"Eläinpuisto-Zoo","ENO":"Eno","EPO":"Espoo","HPJ":"Haapajärvi","HPK":"Haapamäki","HAA":"Haarajoki","HKS":"Hankasalmi","HNK":"Hanko","HKP":"Hanko-Pohjoinen","HVA":"Harjavalta","HAU":"Haukivuori","HNV":"Heinävesi","HKI":"Helsinki","HR":"Herrala","HKH":"Hiekkaharju","HK":"Hikiä","HP":"Humppila","HPL":"Huopalahti","HY":"Hyvinkää","HL":"Hämeenlinna","HÖL":"Höljäkkä","ILM":"Iisalmi","ITA":"Iittala","ILA":"Ilmala","IMR":"Imatra","IKR":"Inkeroinen","IKO":"Inkoo","IKY":"Isokyrö","JNS":"Joensuu","JK":"Jokela","JRS":"Jorvas","JTS":"Joutseno","JJ":"Juupajoki","JY":"Jyväskylä","JÄS":"Jämsä","JR":"Järvelä","JP":"Järvenpää","KAJ":"Kajaani","KAN":"Kannelmäki","KNS":"Kannus","KR":"Karjaa","KRU":"Karkku","KHA":"Kauhava","KLH":"Kauklahti","KNI":"Kauniainen","KA":"Kausala","KEM":"Kemi","KJÄ":"Kemijärvi","KEA":"Kera","KE":"Kerava","KIÄ":"Kerimäki","KTI":"Kesälahti","KEU":"Keuruu","KIL":"Kilo","KKN":"Kirkkonummi","KIT":"Kitee","KRV":"Kiuruvesi","KOH":"Kohtavaara","KVH":"Koivuhovi","KVY":"Koivukylä","KKI":"Kokemäki","KOK":"Kokkola","KLI":"Kolari","KLO":"Kolho","KON":"Kontiomäki","KRA":"Koria","KRS":"Korso","KTA":"Kotka","KTS":"Kotkan satama","KV":"Kouvola","KUO":"Kuopio","KUT":"Kupittaa","KYN":"Kylänlahti","KY":"Kymi","KLN":"Kyminlinna","KRÖ":"Kyrölä","KÄP":"Käpylä","LH":"Lahti","LAI":"Laihia","LNA":"Lapinlahti","LR":"Lappeenranta","LAA":"Lappila","LPO":"Lappohja","LPA":"Lapua","LPÄ":"Lempäälä","LPV":"Leppävaara","LIS":"Lieksa","LVT":"Lievestuore","LM":"Loimaa","LOH":"Louhela","LMA":"Luoma","LUS":"Lusto","ML":"Malmi","MLO":"Malminkartano","MNK":"Mankki","MRL":"Martinlaakso","MAS":"Masala","MI":"Mikkeli","MLA":"Mommila","MH":"Muhos","MUL":"Muurola","MKI":"Myllykoski","MY":"Myllymäki","MYR":"Myyrmäki","MÄK":"Mäkkylä","MLÄ":"Mäntsälä","MR":"Mäntyharju","NSL":"Nastola","NVL":"Nivala","NOA":"Nokia","NUP":"Nuppulinna","NRM":"Nurmes","OI":"Oitti","OV":"Orivesi","OVK":"Orivesi keskusta ","OU":"Oulainen","OL":"Oulu","OLK":"Oulunkylä","PTI":"Paimenportti","PTO":"Paltamo","PAR":"Parikkala","PKO":"Parkano","PRL":"Parola","PSL":"Pasila","PEL":"Pello","PVI":"Petäjävesi","PM":"Pieksämäki","PTS":"Pietarsaari","PH":"Pihlajavesi","PJM":"Pitäjänmäki","POH":"Pohjois-Haaga","PRI":"Pori","PLA":"Puistola","PMK":"Pukinmäki","PUN":"Punkaharju","PUR":"Purola","PHÄ":"Pyhäsalmi","PNÄ":"Pännäinen","PKY ":"Pääskylahti ","RKL":"Rekola","REE":"Retretti","RI":"Riihimäki","ROI":"Rovaniemi","RNN":"Runni","RKI":"Ruukki","RY":"Ryttylä","SLO":"Salo","STA":"Santala","SAU":"Saunakallio","SAV":"Savio","SL":"Savonlinna","SK":"Seinäjoki","SIJ":"Siilinjärvi","SPL":"Simpele","STI":"Siuntio","SKY":"Skogby","SKV":"Sukeva","SNJ":"Suonenjoki","TMS":"Tammisaari","TPE":"Tampere","TNA":"Tapanila","TSL":"Tavastila","TK":"Tervajoki","TRV":"Tervola","TKL":"Tikkurila","TL":"Toijala","TOL":"Tolsa","TOR":"Tornio","TRL":"Tuomarila","TU":"Turenki","TKU":"Turku","TUS":"Turku satama","TUU":"Tuuri","UIM":"Uimaharju","UTJ":"Utajärvi","VAA":"Vaala","VS":"Vaasa","VNA":"Vainikkala","VMO":"Valimo","VMA":"Vammala","VKS":"Vantaankoski","VAR":"Varkaus","VTI":"Vihanti","VIH":"Vihtari","VIA":"Viiala","VNJ":"Viinijärvi","VLP":"Vilppula","VSL":"Vuonislahti","YST":"Ylistaro","YTR":"Ylitornio","YV":"Ylivieska","ÄHT":"Ähtäri"};
//var stationCoordinates = {"ÄHT":{"lat":62.5532679515096,"lon":24.0640359375581},"YV":{"lat":64.0719084706505,"lon":24.5406082756271},"YTR":{},"YST":{"lat":62.9178108284092,"lon":22.5327238574789},"VSL":{"lat":63.1526331577559,"lon":29.9933446623504},"VLP":{"lat":62.0258959837054,"lon":24.5069526240129},"VNJ":{"lat":62.6416863233859,"lon":29.2371844481763},"VIA":{"lat":61.2117248684532,"lon":23.7707179950548},"VIH":{"lat":62.3807426922865,"lon":29.029276382526},"VTI":{"lat":64.4845180676582,"lon":24.9936450586793},"VAR":{"lat":62.3122781129882,"lon":27.8737114038927},"VKS":{"lat":60.2856766141148,"lon":24.8482533138495},"VMA":{"lat":61.3495274748078,"lon":22.8876503013428},"VMO":{"lat":60.2221706066547,"lon":24.8758168247687},"VNA":{"lat":60.8638288370666,"lon":28.3032018341785},"VS":{"lat":63.0978369498149,"lon":21.621702694183},"VAA":{"lat":64.5580173622428,"lon":26.8427111075193},"UTJ":{"lat":64.7589302078738,"lon":26.4135783684134},"UIM":{"lat":62.9124713070708,"lon":30.2415058046907},"TUU":{"lat":62.6053592372922,"lon":23.7223172256872},"TUS":{"lat":60.4344209751372,"lon":22.2249554365505},"TKU":{"lat":60.4540626965237,"lon":22.252813854496},"TU":{"lat":60.9184682054281,"lon":24.638553612264},"TRL":{"lat":60.206054027033,"lon":24.6817589572196},"TOR":{},"TOL":{"lat":60.1176408999754,"lon":24.4702512523943},"TL":{"lat":61.1705965206429,"lon":23.8608235186115},"TKL":{"lat":60.2924319777477,"lon":25.0441779254748},"TRV":{"lat":66.0818108064357,"lon":24.7722940012463},"TK":{"lat":63.0007767284291,"lon":22.1714621079558},"TSL":{"lat":60.5728925381998,"lon":26.9542231070042},"TNA":{"lat":60.2641484505336,"lon":25.0302669204121},"TPE":{"lat":61.4976117284836,"lon":23.7740283973695},"TMS":{"lat":59.9795470484717,"lon":23.4434122398778},"SNJ":{"lat":62.6237152019544,"lon":27.1245055963053},"SKV":{"lat":63.8646120423051,"lon":27.4223116207827},"SKY":{},"STI":{"lat":60.1406547501066,"lon":24.2217515170571},"SPL":{"lat":61.4247849443425,"lon":29.3717002269792},"SIJ":{"lat":63.0749576277382,"lon":27.666564732311},"SK":{"lat":62.7919880445709,"lon":22.844227177813},"SL":{"lat":61.8690566079375,"lon":28.8862334357201},"SAV":{"lat":60.3814262233465,"lon":25.0978503323231},"SAU":{"lat":60.487284553531,"lon":25.0655763884306},"STA":{"lat":59.875072842205,"lon":23.1213189681202},"SLO":{"lat":60.3846131663948,"lon":23.1216922720197},"RY":{"lat":60.8181540074306,"lon":24.7581206881206},"RKI":{"lat":64.6603667264114,"lon":25.0982084816437},"RNN":{"lat":63.6020548214946,"lon":26.8865028008271},"ROI":{"lat":66.4976733332296,"lon":25.7053367438949},"RI":{"lat":60.7361307032109,"lon":24.7812247284075},"REE":{"lat":61.8033020344276,"lon":29.2942868293918},"RKL":{"lat":60.3331035932356,"lon":25.0688235466544},"PKY ":{"lat":61.8632439991,"lon":28.9177589850646},"PNÄ":{"lat":63.5956596099097,"lon":22.7911741925907},"PHÄ":{"lat":63.6829397769137,"lon":25.9826423152734},"PUR":{"lat":60.5002583122918,"lon":25.050270730923},"PUN":{"lat":61.7611459961654,"lon":29.3823578574763},"PMK":{"lat":60.242413491032,"lon":24.9936599437636},"PLA":{"lat":60.276599068651,"lon":25.0367970886227},"PRI":{"lat":61.4772173843826,"lon":21.7865966572185},"POH":{"lat":60.2300817843473,"lon":24.8832494405138},"PJM":{"lat":60.2233884103417,"lon":24.859771422591},"PH":{"lat":62.3462247070028,"lon":24.3955920894681},"PTS":{},"PM":{"lat":62.3011418542859,"lon":27.1675123776504},"PVI":{"lat":62.2569732615613,"lon":25.189339647693},"PEL":{},"PSL":{"lat":60.199054881001,"lon":24.9333345869716},"PRL":{"lat":61.050023039725,"lon":24.366320485935},"PKO":{"lat":62.0517776893819,"lon":23.0784636067994},"PAR":{"lat":61.5585493678418,"lon":29.5028831752296},"PTO":{"lat":64.4006131877315,"lon":27.8221873449959},"PTI":{"lat":60.4759368278471,"lon":26.9193401500995},"OLK":{"lat":60.2292125439701,"lon":24.9680810179195},"OL":{"lat":65.0123980716845,"lon":25.4861387415853},"OU":{"lat":64.2694730635655,"lon":24.8209998295012},"OVK":{"lat":61.6739091855047,"lon":24.3698320093638},"OV":{"lat":61.6500999453772,"lon":24.368040220715},"OI":{"lat":60.7898091491224,"lon":25.0213042401763},"NRM":{"lat":63.5410806691513,"lon":29.1372475465544},"NUP":{"lat":60.5293695500533,"lon":25.0187716737947},"NOA":{"lat":61.4810638150964,"lon":23.4991709390577},"NVL":{"lat":63.9363903683797,"lon":24.9607835267595},"NSL":{"lat":60.9355620325492,"lon":25.9354497170809},"MR":{"lat":61.4211051646656,"lon":26.8817497427936},"MLÄ":{"lat":60.6472564851628,"lon":25.3074652329458},"MÄK":{"lat":60.2213059911276,"lon":24.8429694862304},"MYR":{"lat":60.2613242751813,"lon":24.8547359791479},"MY":{"lat":62.5247548373713,"lon":24.2809648134262},"MKI":{"lat":60.7700325560227,"lon":26.787597784989},"MUL":{"lat":66.3708753946856,"lon":25.3751073856932},"MH":{"lat":64.8054024824012,"lon":25.9920155615267},"MLA":{"lat":60.8211726293947,"lon":25.0751269800873},"MI":{"lat":61.6878162432883,"lon":27.2779652903648},"MAS":{"lat":60.1586587763578,"lon":24.5391015403841},"MRL":{"lat":60.2780803315018,"lon":24.8525836426766},"MNK":{"lat":60.1852574352316,"lon":24.5830801641194},"MLO":{"lat":60.2493233553054,"lon":24.8612641620817},"ML":{"lat":60.2518636271413,"lon":25.011838718977},"LUS":{"lat":61.8005437266694,"lon":29.3222736534224},"LMA":{"lat":60.1731050401385,"lon":24.5508733692944},"LOH":{"lat":60.2708048660607,"lon":24.8532848496211},"LM":{"lat":60.8473989167806,"lon":23.0604068677173},"LVT":{"lat":62.2599100487137,"lon":26.1961652556913},"LIS":{"lat":63.318198405343,"lon":30.0164219508081},"LPV":{"lat":60.2194802336929,"lon":24.8134577450556},"LPÄ":{"lat":61.3142915497061,"lon":23.7550914862771},"LPA":{"lat":62.9713382769334,"lon":23.0151218150812},"LPO":{"lat":59.9040553435155,"lon":23.2367477390249},"LAA":{"lat":60.8489550377593,"lon":25.1742362458716},"LR":{"lat":61.0476065142242,"lon":28.195077199272},"LNA":{"lat":63.3606675668124,"lon":27.3935709934478},"LAI":{"lat":62.9901484511189,"lon":22.0034385941334},"LH":{"lat":60.9764738866518,"lon":25.6572467017841},"KÄP":{"lat":60.2203550243764,"lon":24.9464797333388},"KRÖ":{"lat":60.4567606522757,"lon":25.1015090716606},"KLN":{"lat":60.5083470354029,"lon":26.8955507132172},"KY":{"lat":60.5391160320611,"lon":26.9069885395217},"KYN":{"lat":63.3613924673245,"lon":29.751621134797},"KUT":{"lat":60.4502184006399,"lon":22.2971777476904},"KUO":{"lat":62.897220894424,"lon":27.6795406630281},"KV":{"lat":60.8655426741245,"lon":26.7032673776803},"KTS":{"lat":60.468711537983,"lon":26.9404336053719},"KTA":{"lat":60.4643797813782,"lon":26.9286067753117},"KRS":{"lat":60.3509325880371,"lon":25.0782267517225},"KRA":{"lat":60.8474425454102,"lon":26.6022394721459},"KON":{"lat":64.3382209993014,"lon":28.1147135483404},"KLO":{"lat":62.1280493830283,"lon":24.507437049513},"KLI":{},"KOK":{"lat":63.8344384637995,"lon":23.1310904555053},"KKI":{"lat":61.2541809966044,"lon":22.3034295998596},"KVY":{"lat":60.3235212762024,"lon":25.0600059802696},"KVH":{"lat":60.2071474132788,"lon":24.7012687033073},"KOH":{"lat":63.5151282595281,"lon":29.2842921106587},"KRV":{"lat":63.6409144395118,"lon":26.6113373847646},"KIT":{"lat":62.1428645925832,"lon":30.0491055354533},"KKN":{"lat":60.1196405307238,"lon":24.4389860447007},"KIL":{"lat":60.217935119038,"lon":24.7823960686559},"KEU":{"lat":62.255309245685,"lon":24.7073148861924},"KTI":{"lat":61.887466526836,"lon":29.8028751511363},"KIÄ":{"lat":61.8614028463005,"lon":29.1140729705515},"KE":{"lat":60.4025021488988,"lon":25.1055869101125},"KEA":{"lat":60.2165702785059,"lon":24.7556295250905},"KJÄ":{"lat":66.7242759424658,"lon":27.4037352242751},"KEM":{"lat":65.736706203287,"lon":24.5743921976806},"KA":{"lat":60.8853191308073,"lon":26.3295656053891},"KNI":{"lat":60.2119390946602,"lon":24.7308581218048},"KLH":{"lat":60.1894501477597,"lon":24.6003376990427},"KHA":{"lat":63.0996479779108,"lon":23.0352852831675},"KRU":{"lat":61.4433175911428,"lon":23.0491242792771},"KR":{"lat":60.0686205974971,"lon":23.6609497513004},"KNS":{"lat":63.8984315659718,"lon":23.9153823380664},"KAN":{"lat":60.2395608232772,"lon":24.876999501756},"KAJ":{"lat":64.2197172795586,"lon":27.7385659060757},"JP":{"lat":60.4736462568726,"lon":25.0908525460913},"JR":{"lat":60.8673271430942,"lon":25.2746583365616},"JÄS":{"lat":61.8656316150579,"lon":25.1732702065982},"JY":{"lat":62.2403951514995,"lon":25.7526952070214},"JJ":{"lat":61.7967960324578,"lon":24.365042201252},"JTS":{"lat":61.1202910534812,"lon":28.488240903736},"JRS":{"lat":60.1378946392413,"lon":24.5127813696834},"JK":{"lat":60.5528446571929,"lon":24.9705586882541},"JNS":{"lat":62.5995333454642,"lon":29.7770562044315},"IKY":{"lat":62.9510855113052,"lon":22.3947955585747},"IKO":{"lat":60.0615127464171,"lon":23.935824472537},"IKR":{"lat":60.6967925965661,"lon":26.8389649800709},"IMR":{"lat":61.1960347364294,"lon":28.7763212908405},"ILA":{"lat":60.2081567808029,"lon":24.9205839896604},"ITA":{"lat":61.090582532081,"lon":24.140831707455},"ILM":{"lat":63.5601071505716,"lon":27.2011152262921},"HÖL":{"lat":63.4548742738465,"lon":29.4441411260754},"HL":{"lat":61.0026154759784,"lon":24.4782874731282},"HY":{"lat":60.6314568858326,"lon":24.8574407076537},"HPL":{"lat":60.218365990445,"lon":24.8934628002039},"HP":{"lat":60.9195087412278,"lon":23.3627655360884},"HK":{"lat":60.7525846819442,"lon":24.9192030904225},"HKH":{"lat":60.303345928025,"lon":25.0492146984245},"HR":{"lat":60.8969383594794,"lon":25.4756038568887},"HKI":{"lat":60.1720917396682,"lon":24.9412457352685},"HNV":{"lat":62.3825421542214,"lon":28.6433876159328},"HAU":{"lat":62.0256543118971,"lon":27.2026158709574},"HVA":{"lat":61.3097003697258,"lon":22.1332506144457},"HKP":{"lat":59.8301519801011,"lon":22.9883958484171},"HNK":{"lat":59.8268631302601,"lon":22.968289872509},"HKS":{"lat":62.3028174189374,"lon":26.4781367664871},"HAA":{"lat":60.496255392329,"lon":25.133809515854},"HPK":{"lat":62.2464130806991,"lon":24.4551270905068},"HPJ":{"lat":63.7545205391405,"lon":25.3343637030939},"EPO":{"lat":60.2050697178862,"lon":24.6559798867302},"ENO":{"lat":62.8063772669732,"lon":30.1417109895974},"EPZ":{"lat":62.5409780937739,"lon":24.1903782869506},"DRA":{"lat":59.9902253416585,"lon":23.4886538410037},"ALV":{"lat":62.6177746696382,"lon":23.6004278431244}};


function foo(request, apiResponse) {

	var foo = {};
	for (code in stationCodeToCity) {
		var city  = stationCodeToCity[code];
		foo[city] = code;
	}

	apiResponse.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
	//apiResponse.end(JSON.stringify(station), "utf8");
	apiResponse.end(JSON.stringify(foo), "utf8");
}

function arrayOfCities(request, apiResponse) {

	var cities = [];
	for (city in stationCityToCode) {
		cities.push(city);
	}
	cities.sort();
	apiResponse.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
	apiResponse.end(JSON.stringify(cities), "utf8");
}

function adminCoordinatesToStationCodes(request, apiResponse) {
	var numberOfStations = 0;
	var stations = [];
	for (var code in stationCodeToCity) {
		// var fieldContents = stationCodeToCity[field];
		stations.push(code);
		numberOfStations++;
	}

	getStationInfo(stations.pop());

	var stationCoordinates = {};
	function getStationInfo(code) {
		var url = 'http://188.117.35.14/TrainRSS/TrainService.svc/stationInfo?station='+code;
		console.log('handling ' + code);
		rest.get(url).on('complete', function(data, response) {
			xmlParser(data, function (error, rss) {
				if (error) {
					console.log('ERROR getting coordinates for %s', code);

				} else {
					try {
						var coords = rss.channel['georss:point'].split(' ');
						console.log(coords);
						stationCoordinates[code] = {
							lat: parseFloat(coords[0]),
							lon: parseFloat(coords[1])
						}
					} catch (err) {
						stationCoordinates[code] = {};
					}
				}

				var newStationCode = stations.pop();
				if (newStationCode) {
					getStationInfo(newStationCode);
				} else {
					console.log(stationCoordinates);
					apiResponse.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
					apiResponse.end(JSON.stringify(stationCoordinates), "utf8");
				}
			});
		});
	}
}

exports.adminData = function(request, apiResponse) {
	adminCoordinatesToStationCodes(request, apiResponse);
	//foo(request, apiResponse);
	//arrayOfCities(request, apiResponse);
};

exports.getStationCode = function(city) {
	return stationCityToCode[city] || city;
}

exports.getStationCity = function(code) {
	return stationCodeToCity[code] || code;
}

exports.getStationCoordinates = function(code) {
	return stationCoordinates[code];
}

